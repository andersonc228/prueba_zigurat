-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 30-09-2019 a las 01:36:02
-- Versión del servidor: 5.7.27-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_zigurat`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `leads`
--

CREATE TABLE `leads` (
  `nombre` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` int(9) NOT NULL,
  `pais` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `campaing` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `id_source` int(1) NOT NULL,
  `id_medium` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `leads`
--

INSERT INTO `leads` (`nombre`, `email`, `telefono`, `pais`, `campaing`, `id_source`, `id_medium`) VALUES
('Paola Andrea', 'anderson22@hotmail.com', 666666666, 'colombia', 'hola', 1, 3),
('anderson coronado', 'coro@gmia.com', 123456789, 'ccc', 'aaaa', 5, 4),
('Paola Andrea', 'pao@hotmail.co', 666666666, 'colombia', 'hola', 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medium`
--

CREATE TABLE `medium` (
  `id_medium` int(1) NOT NULL,
  `medium` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `medium`
--

INSERT INTO `medium` (`id_medium`, `medium`) VALUES
(1, 'Artículo'),
(2, 'Newsletter'),
(3, 'Banner'),
(4, 'Otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `source`
--

CREATE TABLE `source` (
  `id_source` int(1) NOT NULL,
  `source` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `source`
--

INSERT INTO `source` (`id_source`, `source`) VALUES
(1, 'Facebook'),
(2, 'Twitter'),
(3, 'Web'),
(4, 'Mailing'),
(5, 'Otro');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`email`),
  ADD KEY `source` (`id_source`),
  ADD KEY `medium` (`id_medium`);

--
-- Indices de la tabla `medium`
--
ALTER TABLE `medium`
  ADD PRIMARY KEY (`id_medium`);

--
-- Indices de la tabla `source`
--
ALTER TABLE `source`
  ADD PRIMARY KEY (`id_source`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `medium`
--
ALTER TABLE `medium`
  MODIFY `id_medium` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `source`
--
ALTER TABLE `source`
  MODIFY `id_source` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `leads`
--
ALTER TABLE `leads`
  ADD CONSTRAINT `leads_ibfk_1` FOREIGN KEY (`id_source`) REFERENCES `source` (`id_source`),
  ADD CONSTRAINT `leads_ibfk_2` FOREIGN KEY (`id_source`) REFERENCES `source` (`id_source`) ON DELETE CASCADE,
  ADD CONSTRAINT `leads_ibfk_3` FOREIGN KEY (`id_medium`) REFERENCES `medium` (`id_medium`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
