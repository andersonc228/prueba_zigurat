<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CrudController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("CrudModel");
	}

	public function index($data = NULL)
	{
        //me recoge los datos del controlador para poder mostrarlos en el formulario de inicio
        $data['selSource'] = $this->CrudModel->viewSource();
        $data['selMedium'] = $this->CrudModel->viewMedium();

        //me muestra todos los valores de la BBDD
        $data['leads'] = $this->CrudModel->listLeads();

        $this->load->view('crud_view', $data);
	}

	/**
	 * Funcion que me recupera los datos del formulario
	 */
    public function insertData(){
        $data = $this->input->post();


		$validateData = $this->valideData($data);

		if ($validateData){
			$data = array(
				'error' => $validateData
			);
			$this->index($data);

		}else{
			if ($data['Guardar_Cambios'] == 'Guardar Cambios'){
				$this->guardarCambios($data);
			}else{
				if (isset($data)){
					$name = $data['name_user'];
					$emai = $data['email_user'];
					$phone = $data['phone_user'];
					$county = $data['country_user'];
					$campaing = $data['campaing_user'];
					$source = $data['source_user'];
					$medium = $data['medium_user'];
					$this->CrudModel->insertData($name,$emai,$phone,$county,$campaing,$source,$medium);
					redirect('');
				}
			}
		}


	}

	/**
	 * funcion que me valida los campos
	 */

	public function valideData($field)
	{
			$error = false;

			$name = $field['name_user'];
			$email = $field['email_user'];
			$phone = (int)$field['phone_user'];
			$county = $field['country_user'];
			$campaing = $field['campaing_user'];
			$source = (int)$field['source_user'];
			$medium = (int)$field['medium_user'];

			if (empty($name) || !is_string($name)) {
				return true;
			}
			if (empty($phone) || !is_int($phone) || !filter_var($phone, FILTER_VALIDATE_INT)) {
				return true;
			}
			if (empty($email) || !is_string($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
				return true;
			}
			if (empty($county) || !is_string($county)) {
				return true;
			}
			if (empty($campaing) || !is_string($campaing)) {
				return true;
			}
			if (empty($source)) {
				return true;
			}
			if (empty($medium)) {
				return true;
			}
			return $error;

	}

	public function borrar(){

		$this->CrudModel->borrar($_GET);
		$this->index();

	}

	/**
	 * funcion que me muestra en el formulario los datos obtenidos de la consulta para poder modificarlos
	 */

	public function viewDataModify(){

		$data['fields'] = $this->CrudModel->listDataModify($_GET);

		$data['selSource'] = $this->CrudModel->viewSource();
		$data['selMedium'] = $this->CrudModel->viewMedium();

		//me muestra todos los valores de la BBDD
		$data['leads'] = $this->CrudModel->listLeads();

		$this->load->view('crud_view', $data);

	}


	/**
	 * funcion que me modifica los valores de los campos
	 */

	public function guardarCambios($data)
	{
		$this->CrudModel->actualizar($data);
		$this->index();
	}
}
