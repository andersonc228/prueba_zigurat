<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CrudModel extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
	}

	public function save ($data_post){

    }
    
    /**
     * funcion que me hace la consulta en la BBDD de los medium, para poder mostrarlos
     * 
     * y me retorna sus valores
     */
    public function viewMedium(){

        $query = $this->db->query('select * from medium');
        //retorna todos los valores de la tabla medium
        return $query->result();
        
    }

    /**
     * funcion que me hace la consulta en la BBDD de los source, para poder mostrarlos
     * 
     * y me retorna sus valores
     */
    public function viewSource(){

        $query = $this->db->query('select * from source');
        //retorna todos los valores de la tabla source
        return $query->result();
        
    }

    /**
	 * Funcion que me inserta los datos en la BBDD
	 */

    public function insertData($name, $email, $phone, $country, $campaing, $source, $medium){

    	$arrayDatos = array(
    		'nombre' => $name,
			'email' => $email,
			'telefono' => $phone,
			'pais' => $country,
			'campaing' => $campaing,
			'id_source' => $source,
			'id_medium' => $medium
		);



    	$this->db->insert('leads',$arrayDatos);
		if($data['error'] = $this->db->error()){
			var_dump($data);
			//return $data;
		};

	}

	/**
	 * funcion que me lista la BBDD
	 */

	public function listLeads(){

		$query = $this->db->query('select * from leads');

		return $query->result();
	}

	/**
	 * Funcion que me borra un registro de la BBDD por medio del email
	 */
	public function borrar($dataGet){
		$this->db->where('email',$dataGet['borrar']);
		$this->db->delete('leads');
	}

	/**
	 * funcion que me modifica los datos en la BBDD por medio del email
	 */

	public function listDataModify($dataGet){

		 $email = $dataGet['modificar'];

			$query = "SELECT *
      		  FROM leads 
      		  WHERE email ='$email'";

			$resultados = $this->db->query($query);

			return $resultados->result();

	}

	/**
	 * funcion que me modifica los registros en la BBDD
	 */
	public function actualizar($data){
		$email = $data['email_user'];

		$datosActualizar = array(
			'nombre' => $data['name_user'],
			'email' => $data['email_user'],
			'telefono'=>$data['phone_user'],
			'pais'=>$data['country_user'],
			'campaing'=> $data['campaing_user'],
			'id_source'=> $data['source_user'],
			'id_medium'=>$data['medium_user']
		);

		$this->db->set('nombre', 'email', 'telefono','pais','campaing', 'id_source','id_medium');
		$this->db->where('email', "$email");
		$this->db->update('leads',$datosActualizar);



	}

}


