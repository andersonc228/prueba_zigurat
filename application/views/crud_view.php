<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>CRUD ZIGURAT</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 text-center">
				<h1>Crud Prueba</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="card">
					<div class="card-header">
						<h1>Agregar Elemento</h1>
						<?php if (isset($error)) {
							echo '<h3 class="btn-info">Hay algun error en los campos</h3>';
						} ?>
					</div>
					<div class="card-body">
						<form action="<?=base_url('CrudController/insertData')?>" method="post">
							<div class="form-row">
								<div class="form-group col-md-12">
									<label for="name_user">Nombre</label>
									<input type="text" class="form-control"  name="name_user" placeholder="Nombre" value="<?php if (isset($fields)) {echo $fields[0]->nombre; }?>" >
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="email_user">Email</label>
									<input type="email" class="form-control" name="email_user" placeholder="Email" value="<?php if (isset($fields)) {echo $fields[0]->email;}?>" required>
								</div>
								<div class="form-group col-md-6">
									<label for="phone_user">Telefono</label>
									<input type="number" class="form-control" name="phone_user" placeholder="Telefono" value="<?php if (isset($fields)) {echo $fields[0]->telefono;}?>" required>
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="country_user">Pais</label>
									<input type="country_user" class="form-control" name="country_user" placeholder="Pais" value="<?php if (isset($fields)) {echo $fields[0]->pais;}?>" required>
								</div>
								<div class="form-group col-md-6">
									<label for="campaing_user">Campaing</label>
									<input type="text" class="form-control" name="campaing_user" placeholder="Campaing" value="<?php if (isset($fields)) {echo $fields[0]->campaing;}?>" required>
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="source_user">Source</label>
									<select name="source_user" class="custom-select">
                                        <?php  foreach($selSource as $value) {
											if (isset($fields)) {
												$selected = "";
												if ($value->id_source == $fields[0]->id_source) {
													$selected = 'selected';
												}
												echo "<option value='{$value->id_source}' $selected> $value->source</option>";
												}else{
													echo "<option value='{$value->id_source}' > $value->source</option>";
											}
										}?>
                                    </select>
								</div>
								<div class="form-group col-md-6">
									<label for="medium_user">Medium</label>
									<select name="medium_user" class="custom-select">
										<?php  foreach($selMedium as $value) {
											if (isset($fields)) {
												$selected = "";
												if ($value->id_medium == $fields[0]->id_medium) {
													$selected = 'selected';
												}
												echo "<option value='{$value->id_medium}' $selected> $value->medium</option>";
												}else{
													echo "<option value='{$value->id_medium}'> $value->medium</option>";
											}
										}?>
									</select>
								</div>
							</div>
							<div class="col-md-12 text-center">
								<button type="submit" class="btn btn-primary col-md-5">Guardar Registro</button>
								<input type="submit" value="Guardar Cambios" name="Guardar Cambios" class="btn btn-success col-md-5">
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">
						Elementos agregados
					</div>
					<div class="card-body">
						<?php require_once 'listCrud.php'; ?>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</body>
</html>
